Feature: Lista de clientes de aplicacion banca movil

  @Test2
  Scenario: Consulta de usuario get - con mas de una query param
  Given que configuro las cabeceras para ejecutar la solicitud "https://reqres.in/"
  | parametros   | valor            |
  | Content-Type | application/json |
  When ejecuto la solicitud get con path "api/users" y params
  | parametros | valor |
  | page       | 2     |
  | size       | 50    |
  Then valida status code igual a 200
  And valido que "total" sea igual a 12
  And valido que "data[0].id" sea igual a 7