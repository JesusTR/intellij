package StepDefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class listCustomersStep {
    RequestSpecification request;
    Response response;
    @Given("que configure las cabeceras para ejecutar la consulta {string}")
    public void configurarCabeceraYHost(String host) {
        RestAssured.baseURI = host;
        request = RestAssured.given();

        request.header("Content-Type", "application/json");
    }
    @When("ejecuto la solicited con {string} y body")
    public void ejecuto_la_solicited_con_y_body(String path, String contentBody) {
        response = request.when().body(contentBody)
                .log().all()
                .post(path);
        //System.out.println("response: " + response.asString());
    }
    @Then("valida status code igual a {int}")
    public void valida_status_code_igual_a(Integer status) {
        response.then()
                .log().all()
                .statusCode(status);
    }
    @Then("obtengo el valor de la etiqueta {string}")
    public void obtengo_el_valor_de_la_etiqueta(String property) {
        String valOutput = JsonPath.from(response.asString()).get(property);
        System.out.println("valor de la propiedad " + property + ": " + valOutput);
    }
}
