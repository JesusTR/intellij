package StepDefinition;

import Utilitario.util;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class listProductsStep extends util{
    Response response;
    RequestSpecification request;
    @Given("que configuro las cabeceras para ejecutar la solicitud {string}")
    public void que_configuro_las_cabeceras_para_ejecutar_la_solicitud(String host, DataTable dataTable) {
        List<Map<String,String>> listHeader = dataTable.asMaps(String.class,String.class);

        RestAssured.baseURI = host;
        request = RestAssured.given();

        request.header(listHeader.get(0).get("parametros"), listHeader.get(0).get("valor"));
    }

    @When("ejecuto la solicitud get con path {string} y params")
    public void ejecuto_la_solicitud_get_con_path_y_params(String path, DataTable dataTable) {
        Map<String, Object> formParams;

        List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
        formParams = configurarParametros(list);

        response = request.when().queryParams(formParams)
                .log().all()
                .get(path);

    }

    @Then("valido que {string} sea igual a {int}")
    public void valido_que_sea_igual_a(String property, Integer value) {
        Integer valProperty = JsonPath.from(response.asString()).get(property);
        Assert.assertEquals(valProperty,value);
        System.out.print("response: ");
    }

    @Then("valido status code igual {int}")
    public void valido_status_code_igual(Integer status) {
        response.then()
                .log().all()
                .statusCode(status);
    }
}
