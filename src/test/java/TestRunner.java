import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"json:target/cucumber_json/cucumber.json"},
        features = "src/test/resources/feature",
        glue = "StepDefinition",
        tags = "@Test2"
)

public class TestRunner {

}
